package com.baomidou.mybatisplus.test.sqlserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.test.sqlserver.entity.TestUser;

/**
 * @author sundongkai
 * @since 2022-04-03
 */
public interface TestUserMapper extends BaseMapper<TestUser> {
}
